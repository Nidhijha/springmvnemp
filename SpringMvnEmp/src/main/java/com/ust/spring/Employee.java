package com.ust.spring;

import java.util.List;

public class Employee {
	private int id;
    private String fName;
    private String lName;
    private double salary;
    private Address address;
    private List<String> skills;
    
    //InIt and Destroy method --> demoBean.xml
    public void init() {
    	System.out.println("Inside Employee class in the Init method");
    }
    public void destroy() {
    	System.out.println("Inside Employee class in the destroy method");
    }
    
    //Default Constructor
    public Employee() {
	}
    
	public Employee(int id, String fName, String lName, double salary, Address address, List<String> skills) {
		super();
		this.id = id;
		this.fName = fName;
		this.lName = lName;
		this.salary = salary;
		this.address = address;
		this.skills = skills;
	}
	
	public Employee(int id, String fName, String lName, double salary, Address address) {
		super();
		this.id = id;
		this.fName = fName;
		this.lName = lName;
		this.salary = salary;
		this.address = address;
	}
	@Override
	public String toString() {
		return "\nEmployee [id=" + id + ", fName=" + fName + ", lName=" + lName + ", salary=" + salary + ",\n address="
				+ address + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	public String getlName() {
		return lName;
	}
	public void setlName(String lName) {
		this.lName = lName;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public List<String> getSkills() {
		return skills;
	}
	public void setSkills(List<String> skills) {
		this.skills = skills;
	}
}
