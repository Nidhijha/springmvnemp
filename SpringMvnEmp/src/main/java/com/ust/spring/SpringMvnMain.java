package com.ust.spring;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringMvnMain {

	public static void main(String[] args) {
//		ApplicationContext ctx = new ClassPathXmlApplicationContext("demoBean.xml");
		AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("demoBean.xml");
		Employee emp = (Employee) ctx.getBean("empl");

		 System.out.println(emp);
		 
		 System.out.println(emp.getSkills());
		 
		 //Register this by using Abstract class --> AbstractApplicationContext
		 ctx.registerShutdownHook();
	}

}
